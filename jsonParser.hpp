
#ifndef jsonParser_h
#define jsonParser_h

#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <iostream>
#include <vector>

using namespace std;

class JsonParser
{
	private:
	// Member Variables
	public:
		std::vector<string> _extractJsonMessage(const json_t*);
		std::vector<string> _input_createJsonMessage(const json_t*);
		string _createJsonMessage(std::vector<string>);
		void _jsonMessageValidate(json_t*);
};

#endif