/******************************************************************************
 * Copyright (c) 2015, Belkin Inc. All rights reserved.
 *
 ******************************************************************************/

#ifndef _JSON_OBJECT_H_
#define _JSON_OBJECT_H_

#include <map>
#include <string.h>
#include <stdlib.h>
#include <iostream>

<<<<<<< HEAD
	class JsonObject
	{
	public:
		// JsonObject(std::string const& SenderID, std::multimap<std::string,std::string> const& RcptID,
		// 			std::string const& TypeID, std::multimap<std::string,std::string> const& Data, std::string const& flags);
		~JsonObject();

		const std::string& getSenderID() const;

		const std::multimap<std::string,std::string>& getRcptID() const;

		const std::string& getTypeID() const;

		const std::multimap<std::string,std::string>& getData() const;

		const std::string& getFlags() const;

		void set_SenderID(std::string);

		void set_TypeID(std::string);

		void set_flags(std::string);

		void set_data(std::multimap<std::string, std::string>&);

		void set_RcptID(std::multimap<std::string, std::string>&);

		std::string createJsonMessage(JsonObject object);
	
	private:
		
		std::string m_SenderID;

		std::multimap<std::string,std::string> m_RcptID;

		std::string m_TypeID;

		std::multimap<std::string,std::string> m_Data;

		std::string m_flags;
	};
=======
namespace linksys {
namespace services {

	class JsonObject
	{
	public:
		JsonObject(std::string const& SenderID, std::multimap<std::string,std::string> const& RcptID,
					std::string const& TypeID, std::multimap<std::string,std::string> const& Data, std::string const& flags);
		~JsonObject();

		const std::string& getSenderID() const;
		const std::multimap<std::string,std::string>& getRcptID() const;
		const std::string& getTypeID() const;
		const std::multimap<std::string,std::string>& getData() const;
		const std::string& getFlags() const;
		
	private:
		std::string m_SenderID;
		std::multimap<std::string,std::string> m_RcptID;
		std::string m_TypeID;
		sstd::multimap<std::string,std::string> m_Data;
		std::string m_flags;
	};
}
}
>>>>>>> 1177c4e5415a1e965cd2f1eb90cc2ae780847f22

#endif /* _JSON_OBJECT_H_ */
