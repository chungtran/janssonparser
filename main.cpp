
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <iostream>
#include <vector>
#include "jsonParser.hpp"

#define STR_NULL ""

using namespace std;

int main(int argc, char *argv[])
{
  //Initiate a JsonParser object
  JsonParser json;

  std::vector<string> test(7);
  std::vector<string>::size_type size = test.size();

  std::vector<string> test1(7);
  std::vector<string>::size_type size1 = test1.size();

  json_t *root = json_object();
  json_t *json_arr_RcptID = json_array();
  json_t *json_arr_Data = json_array();
  json_t *json_arr_Root = json_array();

  json_array_append_new( json_arr_RcptID, json_pack("{s:s}", "dev1", "ALL" ));
  json_array_append_new( json_arr_Data, json_pack("{s:s,s:s,s:s}", "command", 
                        "ADVERTISE", "status", "online", "uptime", "10.03.20.25" ));
  
  json_object_set_new( root, "SenderID", json_string("00:25:9C:13:0F:A6"));
  json_object_set_new( root, "RcptID", json_arr_RcptID);
  json_object_set_new( root, "TypeID", json_string("INFO"));
  json_object_set_new( root, "data", json_arr_Data);
  json_object_set_new( root, "flags", json_string(STR_NULL));

  //json_array_append_new( json_arr_Root, root);
  
  char * jsonOutput = json_dumps(root, 0);

  std::cout<<"\nJson Message Extract Function Template Input:"<<endl;
  std::cout << json_dumps(root, JSON_INDENT(2)) << std::endl;

  json_error_t error;
  json_t *parsedFromString = json_loads(jsonOutput, 0, &error);

  free(jsonOutput);
  json_decref(root);
  
  std::cout<<"\nTest Json Message Validate Function:\n"<<endl;
  json._jsonMessageValidate(parsedFromString);

  if(parsedFromString)
  {
      std::cout<<"\nTest Json Message Extract Function :"<<endl;
      test = json._extractJsonMessage(parsedFromString);
      for(int i=0; i<size ; i++)
      {
        std::cout<<"\n"<<test[i];
      }
      std::cout<<"\n";

      std::cout<<"\nJson Message Create Function Template Input :"<<endl;
      test1 = json._input_createJsonMessage(parsedFromString);
      for(int j=0; j<size1 ; j++)
      {
        std::cout<<"\n"<<test1[j];
      }
  }
  else
  {
      std::cout << error.text << std::endl;
  }

  string st = json._createJsonMessage(test1);
  std::cout<<"\nTest Json Message Create Function:\n\n"<<st<<endl;
  json_decref(parsedFromString);

  return 0;
}
