
#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <iostream>
#include <vector>
#include "jsonParser.hpp"

/* Create Json Message Fuction 
   string _createJsonMessage(std::vector<string> v)
   Return : Json Message String
*/

string JsonParser::_createJsonMessage(std::vector<string> v)
{
  std::vector<string>::size_type size = v.size();
  string s;

  json_t *root = json_object();
  json_t *json_arr_RcptID = json_array();
  json_t *json_arr_Data = json_array();
  json_t *json_arr_Root = json_array();

  json_array_append_new( json_arr_RcptID, json_pack("{s:s}", "dev1", v[1].c_str()) );
  json_array_append_new( json_arr_Data, json_pack("{s:s,s:s,s:s}", "command", 
                        v[3].c_str(), "status", v[4].c_str(), "uptime", v[5].c_str()) );
  
  json_object_set_new( root, "SenderID", json_string(v[0].c_str()) );
  json_object_set_new( root, "RcptID", json_arr_RcptID);
  json_object_set_new( root, "TypeID", json_string(v[2].c_str()) );
  json_object_set_new( root, "data", json_arr_Data);
  json_object_set_new( root, "flags", json_string(v[6].c_str()) );

  return s = json_dumps(root, JSON_INDENT(2));

}

/* Extract Json Message Fuction 
   string _extractJsonMessage(const json_t* message)
   Return : Json Object Vector
*/

std::vector<string> JsonParser::_extractJsonMessage(const json_t* message)
{
    std::vector<string> v;
    json_error_t        error;
    char * jsonOutput = json_dumps(message, 0);
    json_t *root      = json_loads(jsonOutput, 0, &error);
    json_t      *RcptID_array_object, *json_object ,*dev1, *test, *data_array_object , *RcptID, *data;
    int          RcptID_array_lenth, data_array_lenth, j, k;

    if(!root)
    {
        fprintf(stderr, "Error: at line %d: %s\n", error.line, error.text);
        exit(-1);
    }

    if(!json_is_object(root))
    {
        fprintf(stderr, "Error: root is not an json object\n");
        json_decref(root);
        exit(-1);
    }

    const char *iter_keys_SenderID;
    json_t *iter_values_SenderID;
    void* iter_SenderID  = json_object_iter_at(root, "SenderID");
    iter_keys_SenderID   = json_object_iter_key(iter_SenderID);
    iter_values_SenderID = json_object_iter_value(iter_SenderID);
    
    char *v0 = (char*)malloc(128);
    memset(v0, 0x00, 128);
    strncpy(v0,iter_keys_SenderID,128);
    strncat(v0," = ",128);
    strncat(v0,json_string_value(iter_values_SenderID),128);
    v.push_back(v0);

    const char *iter_keys_TypeID;
    json_t *iter_values_TypeID;
    void* iter_TypeID  = json_object_iter_at(root, "TypeID");
    iter_keys_TypeID   = json_object_iter_key(iter_TypeID);
    iter_values_TypeID = json_object_iter_value(iter_TypeID);

    char *v1 = (char*)malloc(128);
    memset(v1, 0x00, 128);
    strncpy(v1,iter_keys_TypeID,128);
    strncat(v1," = ",128);
    strncat(v1,json_string_value(iter_values_TypeID),128);
    v.push_back(v1);

    const char *iter_keys_flags;
    json_t *iter_values_flags;
    void* iter_flags  = json_object_iter_at(root, "flags");
    iter_keys_flags   = json_object_iter_key(iter_flags);
    iter_values_flags = json_object_iter_value(iter_flags);

    char *v2 = (char*)malloc(128);
    memset(v2, 0x00, 128);
    strncpy(v2,iter_keys_flags,128);
    strncat(v2," = ",128);
    strncat(v2,json_string_value(iter_values_flags),128);
    v.push_back(v2);

    RcptID = json_array();
    RcptID = json_object_get(root, "RcptID");
    if(json_is_array(RcptID))
    {
        RcptID_array_lenth = json_array_size(RcptID);
        for(j = 0; j < RcptID_array_lenth; j++ )
        {
            RcptID_array_object = json_array_get(RcptID, j);
            const char *iter_keys_RcptID;
            json_t *iter_values_RcptID;
            void* iter = json_object_iter(RcptID_array_object);
            int n = 0; 
            while(iter && n < json_object_size(RcptID_array_object))
            {
                iter_keys_RcptID   = json_object_iter_key(iter);
                iter_values_RcptID = json_object_iter_value(iter);
                iter = json_object_iter_next(root, iter);
                char *v3 = (char*)malloc(128);
                memset(v3, 0x00, 128);
                strncpy(v3,iter_keys_RcptID,128);
                strncat(v3," = ",128);
                strncat(v3,json_string_value(iter_values_RcptID),128);
                v.push_back(v3);
                n++;
            }
        }
    }
    data = json_array();
    data = json_object_get(root, "data");
    if(json_is_array(data))
    {
        data_array_lenth = json_array_size(data);
        for(k = 0; k < data_array_lenth; k++ )
        {
            data_array_object = json_array_get(data, k);
            const char *iter_keys_data;
            json_t *iter_values_data;
            void* iter = json_object_iter(data_array_object);
            int n = 0; 
            while(iter && n < json_object_size(data_array_object))
            {
                iter_keys_data   = json_object_iter_key(iter);
                iter_values_data = json_object_iter_value(iter);
                iter = json_object_iter_next(root, iter);
                char *v4 = (char*)malloc(128);
                memset(v4, 0x00, 128);
                strncpy(v4,iter_keys_data,128);
                strncat(v4," = ",128);
                strncat(v4,json_string_value(iter_values_data),128);
                v.push_back(v4);
                n++;
            }
        }
    }
    return v;
}

/* Json Message Validation Fuction 
   _jsonMessageValidate(json_t* root)
   Return : Result Validation Message
*/

void JsonParser::_jsonMessageValidate(json_t* root)
{

    json_error_t error;
    int check = json_unpack_ex(root, &error, JSON_VALIDATE_ONLY, 
     "{s:s,s:[{s:s}],s:s,s:[{s:s,s:s,s:s}],s:s}", 
     "SenderID", "RcptID", "dev1", "TypeID", "data", "command", "status" ,"uptime", "flags");

    std::cout<<"check = 0 is Valid and vice versa !"<<check<<endl;
    std::cout<<"check : "<<check<<endl;

    if(check == 0)
    {
        std::cout<<"Json Message is Valid !"<<endl;
    }
    else
    {
        std::cout<<"Json Message is Invalid !"<<endl;
    }
}

// Test Extract Json Message

std::vector<string> JsonParser::_input_createJsonMessage(const json_t* message)
{
    std::vector<string> v;
    json_error_t        error;
    char * jsonOutput = json_dumps(message, 0);
    json_t *root      = json_loads(jsonOutput, 0, &error);

    json_t *json_arr_Root = json_array();
    json_array_append_new( json_arr_Root, root);

    if(!json_arr_Root)
    {
        fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
        exit(-1);
    }

    if(!json_is_array(json_arr_Root))
    {
        fprintf(stderr, "error: root is not an array\n");
        json_decref(json_arr_Root);
        exit(-1);
    }

    for(int i = 0; i < json_array_size(json_arr_Root); i++)
    {
        int         RcptID_array_lenth, data_array_lenth, j, k;
        json_t      *rootdata, *SenderID,  *TypeID, *data, *flags, *RcptID ,*nameSenderID,
                    *RcptID_array_object, *json_object ,*dev1, *test, *data_array_object, 
                    *command, *status, *uptime;

        rootdata = json_array_get(json_arr_Root, i);
        if(!json_is_object(rootdata))
        {
            fprintf(stderr, "error!");
            json_decref(json_arr_Root);
            exit(-1);
        }

        SenderID = json_object_get(rootdata, "SenderID");
        if(!json_is_string(SenderID))
        {
            fprintf(stderr, "error! \n" );
            exit(-1);
        }


        TypeID = json_object_get(rootdata, "TypeID");
        if(!json_is_string(TypeID))
        {
            fprintf(stderr, "error! \n");
            exit(-1);
        }


        flags = json_object_get(rootdata, "flags");
        if(!json_is_string(flags))
        {
            fprintf(stderr, "error! \n");
            exit(-1);
        }

        RcptID = json_array();
        RcptID = json_object_get(rootdata, "RcptID");
        if(json_is_array(RcptID))
        {
            RcptID_array_lenth = json_array_size(RcptID);

            for(j = 0; j < RcptID_array_lenth; j++ )
            {
                RcptID_array_object = json_array_get(RcptID, j);
                dev1 = json_object_get(RcptID_array_object, "dev1");
            }
        }

        data = json_array();
        data = json_object_get(rootdata, "data");
        if(json_is_array(RcptID))
        {
            data_array_lenth = json_array_size(data);

            for(k = 0; k < data_array_lenth; k++ )
            {
                data_array_object = json_array_get(data, k);
                command = json_object_get(data_array_object, "command");
                status = json_object_get(data_array_object, "status");
                uptime = json_object_get(data_array_object, "uptime");
            }
        }

        v.push_back(json_string_value(SenderID));
        v.push_back(json_string_value(dev1));
        v.push_back(json_string_value(TypeID));
        v.push_back(json_string_value(command));
        v.push_back(json_string_value(status));
        v.push_back(json_string_value(uptime));
        v.push_back(json_string_value(flags));
    }

    return v;

    json_decref(json_arr_Root);
}

