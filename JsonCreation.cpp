/******************************************************************************
 * Copyright (c) 2015, Belkin Inc. All rights reserved.
 *
 ******************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <jansson.h>
#include <iostream>
#include <vector>
#include "JsonObject.h"

using namespace std;

int main(int argc, char *argv[])
{

  JsonObject J;

  std::multimap<string,string> test_RcptID, test_Data;

  test_RcptID.insert (std::pair<string,string>("dev1","ALL1"));
  test_RcptID.insert (std::pair<string,string>("dev2","ALL2"));
  test_RcptID.insert (std::pair<string,string>("dev3","ALL3"));
  test_Data.insert (std::pair<string,string>("command","ADVERTISE"));
  test_Data.insert (std::pair<string,string>("status","online"));
  test_Data.insert (std::pair<string,string>("uptime","10.03.20.25"));
  test_Data.insert (std::pair<string,string>("command1","SLEEP"));
  test_Data.insert (std::pair<string,string>("status1","offline"));
  test_Data.insert (std::pair<string,string>("uptime1","10.03.20.26"));
  
  J.set_SenderID("10.03.20.25");
  J.set_TypeID("INFO");
  J.set_flags("NULL");
  J.set_RcptID(test_RcptID);
  J.set_data(test_Data);

  std::cout<<"Json Message Object Input :\n"<<endl;

  cout<<J.getSenderID()<<endl;
  cout<<J.getTypeID()<<endl;
  cout<<J.getFlags()<<endl;

  std::multimap<string,string> test_RcptID1, test_Data1;
  test_Data1 = J.getData();
  test_RcptID1 = J.getRcptID();

  for (std::multimap<string,string>::iterator it=test_Data1.begin(); it!=test_Data1.end(); ++it)
  std::cout << (*it).first << " = " << (*it).second << '\n';
  for (std::multimap<string,string>::iterator it=test_RcptID1.begin(); it!=test_RcptID1.end(); ++it)
  std::cout << (*it).first << " = " << (*it).second << '\n';
  
  std::cout<<"\n----> Json Message Generation :\n"<<endl;

  cout<<J.createJsonMessage(J)<<endl;

  return 0;

}

